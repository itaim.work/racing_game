using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneController : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && transform.parent.CompareTag("Spike"))
        {
            transform.parent.gameObject.GetComponent<SpikeController>().Activate();
        }

    }

}
