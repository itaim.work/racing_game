using UnityEngine;
using System;

public class CarControllerAI : CarController
{
    [SerializeField] private Transform target;
    [SerializeField] private bool NeedToDriveBack;
    [SerializeField] float[] Angles;
    [SerializeField] private GameObject[] WayPoints;
    [SerializeField] private int counter = 0;
    [SerializeField] private float Dist, maxDist;

    // Start is called before the first frame update
    void Start()
    {
        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

        horizontalInput = 0;
        verticalInput = 0;

        Array.Sort(WayPoints, CompareByName);
        target = WayPoints[counter++].transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        Dist = Vector3.Distance(transform.position, target.position);
        if (Dist <= maxDist)
        {
            target = WayPoints[counter++].transform;
        }
        if (counter >= WayPoints.Length)
        {
            counter -= WayPoints.Length;
        }
        Vector3 DirectionToMovePosition = (target.position - transform.position).normalized;

        float dot = Vector3.Dot(transform.forward, DirectionToMovePosition);
        if (dot > 0)
        {
            if (Vector3.Distance(GetComponent<Rigidbody>().velocity, Vector3.zero) > 0.5f && !NeedToDriveBack)
            {
                isBreaking = true;
                verticalInput = 0;
            }
            else
            {
                verticalInput += 0.2f * Time.deltaTime;
                isBreaking = false;
                NeedToDriveBack = true;
            }
        }
        else
        {
            if (Vector3.Distance(GetComponent<Rigidbody>().velocity, Vector3.zero) > 0.5f && NeedToDriveBack)
            {
                isBreaking = true;
                verticalInput = 0;
            }
            else
            {
                NeedToDriveBack = false;
                isBreaking = false;
                verticalInput -= 0.2f * Time.deltaTime;
            }
        }
        verticalInput = Mathf.Clamp(verticalInput, -1, 1);

        
        float angleToDir = Vector3.SignedAngle(transform.forward, DirectionToMovePosition, Vector3.up);
        print(angleToDir);

        if(angleToDir<= Angles[0] && angleToDir>=Angles[1])
        {
            horizontalInput = 1f;
        }
        else if(angleToDir<=Angles[1]&& angleToDir >= Angles[2])
        {
            horizontalInput = 0.7f;
        }
        else if (angleToDir <= Angles[2] && angleToDir >= Angles[3])
        {
            horizontalInput = 0.5f;
        }
        else if (angleToDir <= Angles[3] && angleToDir >= Angles[4])
        {
            horizontalInput = 0.3f;
        }

        else if (angleToDir <= -Angles[1] && angleToDir >= -Angles[0])
        {
            horizontalInput = -1f;
        }
        else if (angleToDir <= -Angles[2] && angleToDir >= -Angles[1])
        {
            horizontalInput = -0.7f;
        }
        else if (angleToDir <= -Angles[3] && angleToDir >= -Angles[2])
        {
            horizontalInput = -0.5f;
        }
        else if (angleToDir <= -Angles[4] && angleToDir >= -Angles[3])
        {
            horizontalInput = -0.3f;
        }
        if (angleToDir >= -Angles[5] && angleToDir <= Angles[5])
        {
            horizontalInput = 0;
        }

        


        HandleMotor();
        HandleSteering();
        UpdateWheels();

    }


    public int CompareByName(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }

}
