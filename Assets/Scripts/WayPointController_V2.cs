using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointController_V2 : MonoBehaviour
{
    [SerializeField] private GameManager_v2 GameMangerScript;

    // Start is called before the first frame update
    void Start()
    {
        GameMangerScript = GameObject.Find("GameManager").GetComponent<GameManager_v2>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerCar_V2"))
        {
            GameMangerScript.DecWayPointCount();
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
    }

}
