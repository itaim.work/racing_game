using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointController : MonoBehaviour
{
    [SerializeField] private GameManager GameManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerCar"))
        {
            GameManagerScript.decCheckPointCounter();
            Material CheckPointMat= gameObject.GetComponent<Renderer>().material;
            CheckPointMat.color = Color.red;
        }
    }

}
